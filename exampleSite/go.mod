module github.com/bowman2001/perplex/exampleSite

go 1.24.0

require (
	gitlab.com/danielfdickinson/bowman2001-hugo-mod-material-symbols v0.2.1 // indirect
	gitlab.com/danielfdickinson/bowman2001-perplex v0.21.3 // indirect
)
