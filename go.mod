module gitlab.com/danielfdickinson/bowman2001-perplex

go 1.24.0

require (
	github.com/bowman2001/hugo-mod-error-message v0.4.0 // indirect
	github.com/bowman2001/hugo-mod-katex v0.2.4 // indirect
	github.com/bowman2001/hugo-mod-mermaid v0.4.2 // indirect
	github.com/bowman2001/hugo-mod-pangram v0.0.0-20230803153646-6851d0e94236 // indirect
	github.com/bowman2001/hugo-mod-replacements v0.4.0 // indirect
	github.com/bowman2001/hugo-mod-wrap v0.0.0-20230803072025-a10a9f082aac // indirect
	gitlab.com/danielfdickinson/bowman2001-hugo-mod-image v0.7.3 // indirect
	gitlab.com/danielfdickinson/bowman2001-hugo-mod-material-symbols v0.2.1 // indirect
	gitlab.com/danielfdickinson/bowman2001-hugo-mod-meta v0.2.6 // indirect
	gitlab.com/danielfdickinson/bowman2001-hugo-mod-resource v0.1.6 // indirect
	gitlab.com/danielfdickinson/bowman2001-hugo-mod-simple-icons v0.1.1 // indirect
)
