$hugoEnv = @{
    HUGO_MINIFY_TDEWOLFF_HTML_KEEPCOMMENT = "";
    HUGO_ENABLEMISSINGTRANSLATIONPLACEHOLDERS = "";
    siteConfig = "";
    HUGO_CACHEDIR = "";
    HUGO_RESOURCEDIR = "";
}

function Get-HugoBin {
    Try {
        Set-Variable -Name "hvm_show_status" -Value $true -Scope private
        Set-Variable -Name "hugo_bin" -Value $(hvm status --printExecPathCached) -Scope private

        If ($hugo_bin) {
            If ($hvm_show_status) {
                Write-Host "Hugo version management is enabled in this directory."
                Write-Host "Run 'hvm status' for details, or 'hvm disable' to disable.`n"
            }
        } Else {
            Set-Variable -Name "hugo_bin" -Value $(hvm status --printExecPath)
            If ($hugo_bin) {
                hvm use --useVersionInDotFile
            } Else {
                Set-Variable -Name "hugo_bin" -Value $((Get-Command hugo.exe).Path 2> $null)
                If (-not ($hugo_bin)) {
                    Write-Error -Message "Command not found"
                    exit 1
                }
            }
        }
        return "$hugo_bin"
    } Catch {
        throw $_
    }
}

function Set-HugoEnvironment {
    param (
        $HUGO_MINIFY_TDEWOLFF_HTML_KEEPCOMMENTS,
        $HUGO_ENABLEMISSINGTRANSLATIONPLACEHOLDERS
    )
    Try {
        $hugoEnv["HUGO_MINIFY_TDEWOLFF_HTML_KEEPCOMMENTS"] = "$Env:HUGO_MINIFY_TDEWOLFF_HTML_KEEPCOMMENTS"

        if ($HUGO_MINIFY_TDEWOLFF_HTML_KEEPCOMMENTS -ne "") {
            $Env:HUGO_MINIFY_TDEWOLFF_HTML_KEEPCOMMENTS = "$HUGO_MINIFY_TDEWOLFF_HTML_KEEPCOMMENTS"
        }

        $hugoEnv["HUGO_ENABLEMISSINGTRANSLATIONPLACEHOLDERS"] = "$Env:HUGO_ENABLEMISSINGTRANSLATIONPLACEHOLDERS"

        if ($HUGO_ENABLEMISSINGTRANSLATIONPLACEHOLDERS -ne "") {
            $Env:HUGO_ENABLEMISSINGTRANSLATIONPLACEHOLDERS = "$HUGO_ENABLEMISSINGTRANSLATIONPLACEHOLDERS"
        }

        If  ($Env:SITECONFIG) {
          $hugoEnv["siteConfig"] = "--config", "$Env:SITECONFIG"
        } else {
          $hugoEnv["siteConfig"] = ""
        }

        If ($Env:SITESRC) {
          $hugoEnv["siteSource"] = "--source", "$Env:SITESRC"
        } else {
          $hugoEnv["siteSource"] = ""
        }

        $hugoEnv.HUGO_CACHEDIR = $Env:HUGO_CACHEDIR
        if (-not "Env:HUGO_CACHEDIR") {
          $Env:HUGO_CACHEDIR = Join-Path "$PWD" -ChildPath "hugo-cache"
        }

        $hugoEnv["hasExampleSite"] = $false

        $hugoEnv.HUGO_RESOURCEDIR = $Env:HUGO_RESOURCEDIR
        if ($true -eq (Test-Path -Path (Join-Path . -ChildPath exampleSite))) {
          $hugoEnv["hasExampleSite"] = $true
          Set-Location exampleSite
          if (-not "$Env:HUGO_RESOURCEDIR") {
            $Env:HUGO_RESOURCEDIR = Join-Path (Join-Path -Path $PWD -ChildPath "..") -ChildPath "hugo-resources"
          }
        } else {
          if (-not "$Env:HUGO_RESOURCEDIR") {
            $Env:HUGO_RESOURCEDIR = Join-Path -Path $PWD -ChildPath "hugo-resources"
          }
        }

        if ((-not "$Env:HUGO_MODULE_WORKSPACE") -or ("off" -eq $Env:HUGO_MODULE_WORKSPACE)) {
          $hugoEnv["IgnoreVendorPaths"] = ""
        } else {
          $hugoEnv["IgnoreVendorPaths"] = "--ignoreVendorPaths",  "**"
        }

        if ($Env:BASEURL) {
          $hugoEnv["baseURL"] = "-b", "$Env:BASEURL"
        }
    } Catch {
        throw $_
    }
}

function Reset-HugoEnvironment {
    $Env:HUGO_CACHEDIR = $hugoEnv.HUGO_CACHEDIR
    $Env:HUGO_RESOURCEDIR = $hugoEnv.HUGO_RESOURCEDIR
    $Env:HUGO_MINIFY_TDEWOLFF_HTML_KEEPCOMMENTS = $hugoEnv.HUGO_MINIFY_TDEWOLFF_HTML_KEEPCOMMENTS
    $Env:HUGO_ENABLEMISSINGTRANSLATIONPLACEHOLDERS = $hugoEnv.HUGO_ENABLEMISSINGTRANSLATIONPLACEHOLDERS
}

function Invoke-Hugo {
    param (
        [Parameter()]
        [string]$HugoAuditVars,
        [Parameter(Position=1, ValueFromRemainingArguments)]
        [string[]]$invokeArgs
    )

    $oldPWD = $PWD

    Try {
        $HugoBin = Get-HugoBin

        If  (-not "$Env:HUGO_COMMAND") {
            $hugoArgs = $invokeArgs
        } else {
            $hugoArgs = ($Env:HUGO_COMMAND -replace "hugo.exe ", "") -Split " "
            $hugoArgs = $hugoArgs, $invokeArgs
        }

        $newHugoArgs = @()
        $hugoArgs = $hugoArgs | ForEach-Object { If ((-not (($null -eq $_) -or ("" -eq $_))) -and ($_ -like "--*"  )) { $newHugoArgs += $_ } else { $newHugoArgs += "`"$_`"" } }

        Invoke-Command -ScriptBlock {
            Try {
                If (($null -eq $HugoAuditVars) -or ($HugoAuditVars -eq "")) {
                    Set-HugoEnvironment
                } else {
                    Set-HugoEnvironment -HUGO_MINIFY_TDEWOLFF_HTML_KEEPCOMMENTS "$HugoAuditVars" -HUGO_ENABLE_MISSINGTRANSLATIONPLACEHOLDERS "$HugoAuditVars"
                }
                & "$HugoBin" @newhugoArgs @Hugo:baseURL $Hugo:IgnoreVendorPaths @Hugo:siteConfig @Hugo:siteSource @Hugo:IgnoreVendorPaths 2>&1
                Reset-HugoEnvironment
            } Catch {
                throw $_
            }
        }
    } Catch {
        throw $_
    } Finally {
        Set-Location $oldPWD
    }
}

function Get-HugoDestination {
    Try {
        If (-not "$Env:SITEROOT") {
            $siteRoot = $PWD
        } else {
            $siteRoot = "$Env:SITEROOT"
        }
        $destination = Join-Path -Path $siteRoot -ChildPath "public"
        return $destination
    } Catch {
        throw $_
    }

    return $siteRoot
}

Export-ModuleMember -Function Get-HugoBin, Invoke-Hugo, Get-HugoDestination
