function Get-ScriptPath()
{
    # Get-ScriptPath from https://stackoverflow.com/a/58768926

    # If using PowerShell ISE
    if ($psISE)
    {
        $ScriptPath = Split-Path -Parent -Path $psISE.CurrentFile.FullPath
    }
    # If using PowerShell 3.0 or greater
    elseif($PSVersionTable.PSVersion.Major -gt 3)
    {
        $ScriptPath = $PSScriptRoot
    }
    # If using PowerShell 2.0 or lower
    else
    {
        $ScriptPath = split-path -parent $MyInvocation.MyCommand.Path
    }

    # If still not found
    # I found this can happen if running an exe created using PS2EXE module
    if(-not $ScriptPath) {
        $ScriptPath = [System.AppDomain]::CurrentDomain.BaseDirectory.TrimEnd('\')
    }

    # Return result
    return $ScriptPath
}

$regexPattern = '<\!-- raw HTML omitted -->|ZgotmplZ|hahahugo|\[i18n\]|\(<nil>\)|\(&lt;nil&gt;\)'
$falsePositiveRegexPattern = 'grep.+((-- raw HTML omitted --|ZgotmplZ|hahahugo|\\\[i18n\\\]|\\\(<nil>\\\)|\\\(&lt;nil&gt;\\\)).*)+'

$scriptRoot = Get-ScriptPath

Import-Module $scriptRoot\..\..\scripts\psmod\invoke-hugo.psm1

function Invoke-Audit {
  Try {
    $retCode = 0
    $destination = Get-HugoDestination

    Try {
      Write-Host "Building for audit in $destination"
      Remove-Item -Recurse -Force -Path $destination -ErrorAction SilentlyContinue
      $hugoOutput = (Invoke-Hugo -HugoAuditVars "true" --gc --buildDrafts --buildFuture "--buildDrafts" "--buildFuture" "--destination" "$destination" --environment "development")
    } Catch {
      throw $_
    }

    If ($hugoOutput) {
      Write-Host ($hugoOutput -Join "`n")
      $lines = $hugoOutput -Split "`n" | Select-String -Pattern $regexPattern -CaseSensitive:$false | Select-Object Filename, LineNumber, Line, Path
      if ($lines.Count -ne 0) {
        $retCode = 1
      }
    } else {
      Write-Error "no output from hugo command"
      exit 1
    }

    if ($retCode -ne 0) {
      Write-Output "not ok"
      exit $retCode
    }

    $lines2 = Get-ChildItem -Path $destination -Recurse -Filter '*.html' | ForEach-Object { Get-Content -Path $_.FullName | Select-String -Pattern $falsePositiveRegexPattern -CaseSensitive:$false } | Select-Object Filename, LineNumber, Line, Path
    if ($lines2.Count -ne 0) {
      $lines2 | ForEach-Object { $_.Line } | Write-Host -ForegroundColor Red
      Write-Output "not ok"
      exit 1
    }

    Try {
      Write-Host "Building unminified site without audit only information"
      Remove-Item -Recurse -Force -Path $destination -ErrorAction SilentlyContinue
      $hugoOutput = (Invoke-Hugo -HugoAuditVars "false" "$hugo_bin" --gc --buildDrafts --buildFuture --environment "development" "--buildDrafts" "--buildFuture" "--destination" "$destination" --printPathWarnings --printI18nWarnings)
    } Catch {
      throw $_
    }

    If ($hugoOutput) {
      Write-Host ($hugoOutput -Join "`n")
      If ($true -eq (Test-Path (Join-Path ($destination) -ChildPath "index.html"))) {
        $regexPattern = "(WARN [0-9: ]+|FAIL: )"
        $lines = $hugoOutput -Split "`n" | Select-String -Pattern $regexPattern -CaseSensitive:$false | Select-Object Filename, LineNumber, Line, Path
      } else {
        Write-Error "Missing index.html for home page"
        Write-Output "not ok"
        exit 1
      }
    } else {
      Write-Error "no output from hugo command"
      Write-Host -ForegroundColor red $hugoOutput
      exit 1
    }

    if ($lines.Count -ne 0) {
      $lines | ForEach-Object { $_.Line } | Write-Host -ForegroundColor Red
      Write-Output "not ok"
      exit 1
    }

    Write-Output "ok"
  } Catch {
    Write-Error -Message "`nScripting error in Hugo audit"
    Write-Output "not ok"
    throw $_
  }
}

Try {
  Invoke-Audit
} Catch {
  throw $_
} Finally {
    Remove-Module invoke-hugo
}
