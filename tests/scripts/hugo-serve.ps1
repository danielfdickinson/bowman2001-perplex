function Get-ScriptPath()
{
    # Get-ScriptPath from https://stackoverflow.com/a/58768926

    # If using PowerShell ISE
    if ($psISE)
    {
        $ScriptPath = Split-Path -Parent -Path $psISE.CurrentFile.FullPath
    }
    # If using PowerShell 3.0 or greater
    elseif($PSVersionTable.PSVersion.Major -gt 3)
    {
        $ScriptPath = $PSScriptRoot
    }
    # If using PowerShell 2.0 or lower
    else
    {
        $ScriptPath = split-path -parent $MyInvocation.MyCommand.Path
    }

    # If still not found
    # I found this can happen if running an exe created using PS2EXE module
    if(-not $ScriptPath) {
        $ScriptPath = [System.AppDomain]::CurrentDomain.BaseDirectory.TrimEnd('\')
    }

    # Return result
    return $ScriptPath
}

$scriptRoot = Get-ScriptPath

Import-Module $scriptRoot\..\..\scripts\psmod\invoke-hugo.psm1

$hugoBaseURL = $Env:BASEURL
$Env:BASEURL = ""

Try {
  Invoke-Hugo "serve" "--buildDrafts" "--buildFuture"
} Finally {
  Remove-Module invoke-hugo
  $Env:BASEURL = $hugoBaseURL
}
