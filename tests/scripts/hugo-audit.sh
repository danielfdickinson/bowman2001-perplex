#!/bin/bash

set -e
set -o pipefail

if [ -z "$HUGO_COMMAND" ]; then
  export HUGO_COMMAND="hugo"
fi

if [ -z "$SITEROOT" ]; then
  export SITEROOT="$(pwd)"
fi

if [ -n "$SITECONFIG" ]; then
  export SITECONFIG="--config $SITECONFIG"
fi

if [ -n "$SITESRC" ]; then
  export SITESRC="--source $SITESRC"
fi

if [ -z "${HUGO_CACHEDIR}" ]; then
  HUGO_CACHEDIR="$(pwd)/hugo-cache"
fi

export HUGO_CACHEDIR

if [ -d exampleSite ]; then
  cd exampleSite
  if [ -z "$HUGO_RESOURCEDIR" ]; then
    export HUGO_RESOURCEDIR="$(pwd)/../hugo-resources"
  fi
else
  if [ -z "$HUGO_RESOURCEDIR" ]; then
    export HUGO_RESOURCEDIR="$(pwd)/hugo-resources"
  fi
fi

if [ -z "$HUGO_MODULE_WORKSPACE" ] || [ "$HUGO_MODULE_WORKSPACE" == "off" ]; then
  unset IGNOREVENDORPATHS
  export IGNOREVENDORPATHS
else
  export IGNOREVENDORPATHS="--ignoreVendorPaths \"**\""
fi

if [ -z "$HUGO_ENVIRONMENT" ]; then
  if [ -z "$HUGO_ENV" ]; then
    export HUGO_ENV="development"
  fi
  export HUGO_ENVIRONMENT="$HUGO_ENV"
else
  export HUGO_ENV="$HUGO_ENVIRONMENT"
fi

if [ -n "$BASEURL" ]; then
  BASEURL="-b $BASEURL"
fi

rm -rf "${SITEROOT}/public"

echo "Building for audit in ${SITEROOT}/public for environment ${HUGO_ENVIRONMENT}"

# shellcheck disable=2086
if HUGO_MINIFY_TDEWOLFF_HTML_KEEPCOMMENTS=true HUGO_ENABLEMISSINGTRANSLATIONPLACEHOLDERS=true $HUGO_COMMAND $SITECONFIG $SITESRC --gc --buildDrafts --buildFuture --destination "${SITEROOT}/public" $BASEURL $IGNOREVENDORPATHS && [ -s "${SITEROOT}/public/index.html" ]; then
  # If hugo build succeeds, it is possible audit issues are present, check further
  # Check for problem indicators (see https://discourse.gohugo.io/t/audit-your-published-site-for-problems/35184)
  set +e
  grep -iIrnE '<\!-- raw HTML omitted -->|ZgotmplZ|hahahugo|\[i18n\]|\(<nil>\)|\(&lt;nil&gt;\)' "${SITEROOT}/public/" >hugo-audit.log
  RET=$?
  set -e
  if [ "$RET" != "1" ] && [ "$RET" != "0" ]; then
    # Make sure we fail if there is is error executing the check command
    echo "not ok"
    exit 1
  fi

  # Check if problem indicators are part of some page's content (e.g. a page describing how
  # to check for Hugo audit issues).
  if [ "$RET" = "0" ] && [ -s hugo-audit.log ]; then
    set +e
    grep -iIvE 'grep.+((-- raw HTML omitted --|ZgotmplZ|hahahugo|\\\[i18n\\\]|\\\(<nil>\\\)|\\\(&lt;nil&gt;\\\)).*)+' hugo-audit.log
    RET2=$?
    set -e
    if [ "$RET2" != "1" ]; then
      # Make sure we fail if there is an error executing the false positive elimination command (exit code 2)
      # Also fail if there was a true positive (exit code 0)
      echo "not ok"
      exit 1
    fi
  fi
  rm -rf "${SITEROOT}/public"
  echo "Building unminified site for checks in ${SITEROOT}/public for environment ${HUGO_ENV:-development}"
  # Build unminified site without audit-only information
  if HUGO_MINIFY_TDEWOLFF_HTML_KEEPCOMMENTS=false HUGO_ENABLEMISSINGTRANSLATIONPLACEHOLDERS=false $HUGO_COMMAND $SITECONFIG $SITESRC --gc --buildDrafts --buildFuture --destination "${SITEROOT}/public" $BASEURL $IGNOREVENDORPATHS  --printPathWarnings --printUnusedTemplates --printI18nWarnings >hugo-build-unminified.log; then
    if [ -s "${SITEROOT}/public/index.html" ]; then
      set +e
      grep -InE '(WARN [0-9:/\ ]+ FAIL: )' hugo-build-unminified.log
      RET3=$?
      set -e
      if [ "$RET3" = "0" ]; then
        echo "not ok"
        exit 1
      fi
    else
      echo "not ok"
      exit 1
    fi
    echo "ok"
    exit 0
  else
    # If Hugo build fails, audit fails
    echo "not ok"
    exit 1
  fi
else
  # If Hugo build fails, audit fails
  echo "not ok"
  exit 1
fi
