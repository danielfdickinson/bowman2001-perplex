#!/bin/bash

set -e
set -o pipefail

if [ -z "$HUGO_COMMAND" ]; then
  export HUGO_COMMAND="hugo"
fi

if [ -z "$SITEROOT" ]; then
  export SITEROOT="$(pwd)"
fi

if [ -n "$SITECONFIG" ]; then
  export SITECONFIG="--config $SITECONFIG"
fi

if [ -n "$SITESRC" ]; then
  export SITESRC="--source $SITESRC"
fi

if [ -z "${HUGO_CACHEDIR}" ]; then
  HUGO_CACHEDIR="$(pwd)/hugo-cache"
fi

export HUGO_CACHEDIR

if [ -d exampleSite ]; then
  cd exampleSite
  if [ -z "$HUGO_RESOURCEDIR" ]; then
    export HUGO_RESOURCEDIR="$(pwd)/../hugo-resources"
  fi
else
  if [ -z "$HUGO_RESOURCEDIR" ]; then
    export HUGO_RESOURCEDIR="$(pwd)/hugo-resources"
  fi
fi

if [ -z "$HUGO_MODULE_WORKSPACE" ] || [ "$HUGO_MODULE_WORKSPACE" == "off" ]; then
  unset IGNOREVENDORPATHS
  export IGNOREVENDORPATHS
else
  export IGNOREVENDORPATHS="--ignoreVendorPaths \"**\""
fi

if [ -z "$HUGO_ENVIRONMENT" ]; then
  if [ -z "$HUGO_ENV" ]; then
    export HUGO_ENV="development"
  fi
  export HUGO_ENVIRONMENT="$HUGO_ENV"
else
  export HUGO_ENV="$HUGO_ENVIRONMENT"
fi

if [ -n "$BASEURL" ]; then
  BASEURL="-b $BASEURL"
fi

exec $HUGO_COMMAND serve $SITECONFIG $SITESRC --buildDrafts --buildFuture --destination "${SITEROOT}/public" $IGNOREVENDORPATHS
