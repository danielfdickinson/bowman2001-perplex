$OutputEncoding = [Console]::InputEncoding = [Console]::OutputEncoding =
  New-Object System.Text.UTF8Encoding

$htmlFiles = Get-ChildItem -Path .\public -Filter *.html -Recurse -ErrorAction SilentlyContinue -Force | ForEach-Object {$_.FullName}
$cssFiles = Get-ChildItem -Path .\public -Filter *.css -Recurse -ErrorAction SilentlyContinue -Force | ForEach-Object {$_.FullName}

if ($Env:USERPROFILE) {
  $jarPath = Join-Path (Join-Path -Path "$Env:USERPROFILE" -ChildPath "jar") -ChildPath "vnu.jar"
} Elseif ($HOME) {
  $jarPath = Join-Path (Join-Path -Path "$HOME" -ChildPath "jar") -ChildPath "vnu.jar"
}

if ($jarPath) {
  $validatorOutput = (& java -jar "$jarPath" --Werror --no-langdetect --skip-non-html $htmlFiles 2>&1)
  if (-not ($validatorOutput)) {
    $validatorOutput = (& java -jar "$jarPath" --Werror --skip-non-css $cssFiles 2>&1)
    if (-not ($validatorOutput)) {
      Write-Output "ok"
    } else {
      Write-Host -ForegroundColor Red ($validatorOutput -Join "`n")
      Write-Output "not ok"
      exit 1
    }
  } else {
    Write-Host -ForegroundColor Red ($validatorOutput -Join "`n")
    Write-Output "not ok"
    exit 1
  }
} else {
  Write-Error "Unable to find validator .jar"
}
