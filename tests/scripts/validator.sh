#!/bin/bash

htmlFiles="$(find $(pwd)/public -name '*.html')"
cssFiles="$(find $(pwd)/public -name '*.css')"

java -jar ~/jar/vnu.jar --skip-non-html $(echo "$htmlFiles" | tr $'\n' ' ') 2>validator-html-raw.log
java -jar ~/jar/vnu.jar --stdout --skip-non-css $(echo "$cssFiles" | tr $'\n' ' ') 2>validator-css-raw.log

cat validator-html-raw.log >validator-html.log
cat validator-css-raw.log >validator-css.log

RET=0

if [ -s validator-html.log ]; then
  cat validator-html.log >&2
  RET=1
fi

if [ -s validator-css.log ]; then
  cat validator-css.log >&2
  RET=1
fi

if [ "$RET" != "0" ]; then
  echo "not ok"
  exit 1
fi

echo ok
exit 0
